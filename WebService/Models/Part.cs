//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebService.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Part
    {
        public string part_no { get; set; }
        public string part_name { get; set; }
        public string model { get; set; }
        public Nullable<decimal> cav { get; set; }
        public Nullable<decimal> weight { get; set; }
        public Nullable<decimal> gram_pcs { get; set; }
        public Nullable<decimal> gram_runner { get; set; }
        public Nullable<decimal> gram_shot { get; set; }
        public Nullable<decimal> price { get; set; }
        public Nullable<decimal> sec_shot { get; set; }
        public string machine_no { get; set; }
        public string material_no { get; set; }
    }
}
