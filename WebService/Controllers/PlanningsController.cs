﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebService.Models;

namespace WebService.Controllers
{
    public class PlanningsController : ApiController
    {
        private THAIARROW1STEntities db = new THAIARROW1STEntities();

        // GET: api/Plannings
        public IQueryable<Planning> GetPlannings()
        {
            return db.Plannings;
        }

        // GET: api/Plannings/5
        [ResponseType(typeof(Planning))]
        public IHttpActionResult GetPlanning(long id)
        {
            Planning planning = db.Plannings.Find(id);
            if (planning == null)
            {
                return NotFound();
            }

            return Ok(planning);
        }

        // PUT: api/Plannings/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPlanning(long id, Planning planning)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != planning.planning_id)
            {
                return BadRequest();
            }

            db.Entry(planning).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlanningExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Plannings
        [ResponseType(typeof(Planning))]
        public IHttpActionResult PostPlanning(Planning planning)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Plannings.Add(planning);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = planning.planning_id }, planning);
        }

        // DELETE: api/Plannings/5
        [ResponseType(typeof(Planning))]
        public IHttpActionResult DeletePlanning(long id)
        {
            Planning planning = db.Plannings.Find(id);
            if (planning == null)
            {
                return NotFound();
            }

            db.Plannings.Remove(planning);
            db.SaveChanges();

            return Ok(planning);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PlanningExists(long id)
        {
            return db.Plannings.Count(e => e.planning_id == id) > 0;
        }
    }
}