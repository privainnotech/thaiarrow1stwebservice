﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebService.Models;
using log4net;
using System.Web;

namespace WebService.Controllers
{
    public class MachinesController : ApiController
    {
        private THAIARROW1STEntities db = new THAIARROW1STEntities();
        private readonly ILog logger = LogManager.GetLogger(typeof(MachinesController));
        string clientAddress = HttpContext.Current.Request.UserHostAddress;

        // GET: api/Machines
        public IQueryable<Machine> GetMachines()
        {
            logger.Info("GET GetMachines");
            return db.Machines;
        }

        // GET: api/Machines/5
        [ResponseType(typeof(Machine))]
        public IHttpActionResult GetMachine(string id)
        {
            logger.Info("GET GetMachines by ID");
            Machine machine = db.Machines.Find(id);
            if (machine == null)
            {
                return NotFound();
            }

            return Ok(machine);
        }

        // PUT: api/Machines/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMachine(string id, Machine machine)
        {
            logger.Info("PUT GetMachines");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != machine.machine_no)
            {
                return BadRequest();
            }

            db.Entry(machine).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MachineExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Machines
        [ResponseType(typeof(Machine))]
        public IHttpActionResult PostMachine(Machine machine)
        {
            logger.Info("POST GetMachines");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Machines.Add(machine);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (MachineExists(machine.machine_no))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = machine.machine_no }, machine);
        }

        // DELETE: api/Machines/5
        [ResponseType(typeof(Machine))]
        public IHttpActionResult DeleteMachine(string id)
        {
            logger.Info("DELETE GetMachines");
            Machine machine = db.Machines.Find(id);
            if (machine == null)
            {
                return NotFound();
            }

            db.Machines.Remove(machine);
            db.SaveChanges();

            return Ok(machine);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MachineExists(string id)
        {
            return db.Machines.Count(e => e.machine_no == id) > 0;
        }
    }
}